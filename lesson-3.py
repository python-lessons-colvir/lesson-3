#  |
goods = {
    'Чай': '12345',
    'Булка французкая': '23456',
    'Вино': '34567',
    'Килька запорожская': '45678',
}
store = {
    '12345': [
        {'quantity': 30, 'price': 42},
        {'quantity': 14, 'price': 222},
    ],
    '23456': [
        {'quantity': 22, 'price': 510},
    ],
    '34567': [
        {'quantity': 17, 'price': 1100},
        {'quantity': 10, 'price': 1000},
    ],
    '45678': [
        {'quantity': 50, 'price': 90},
        {'quantity': 12, 'price': 195},
        {'quantity': 43, 'price': 197},
    ],
}

for i in goods.values():
    for good_index in store:
        if i == good_index:
            sumCount = 0
            for quant in store.get(i):
                arr = []
                for quantity in quant.values():
                    arr.append(quantity)
                sum = arr[1] * arr[0]
                if sumCount == 0:
                    sumCount = sum
                elif sumCount != 0:
                    sumCount += sum
    print("Code of goods: " + i +" = "+ str(sumCount))

######################################
#Code wars-1
word = "fjksdk"

def duplicate_encode(word):
    string = ""
    print(string.join(["(" if word.lower().count(x) == 1 else ")" for x in word.lower()]))

print(duplicate_encode(word))

#Code wars-2
text = "The sunset sets at twelve o' clock."

def alphabet_position(text):
    alp = "abcdefghijklmnopqrstuvwxyz"
    string = " "
    arr = []
    for c in text.lower():
        if c in alp:
            a = string.join([str(alp.find(c) + 1)])
            arr.append(a) 
            b = string.join(arr)
    print(b)   

alphabet_position(text)

#Code wars-3
aaa = [121, 144, 19, 161, 19, 144, 19, 11]  
bbb = [121, 14641, 20736, 361, 25921, 361, 20736, 361, 23]

# this option work good
from collections import Counter
def comp(array1, array2):
    comp = lambda a, b: a != None and b != None and Counter(map(lambda x: x*x,a))==Counter(b)

# this option pass 104 atempts
    for i in array1:
        squa = 0
        squa = i * i
        for a in array2:
            if a == squa:
                indexing = (array2.index(a))
                string = str(i) +"*"+ str(i)
                array2[indexing] = str(string)
    print(array1)
    print(array2)
    check = True
    if array2 == None and array1 == []:
        check = False
        return check
    elif array1 == None and array2 == []:
        check = False
        return check
        
        
    for arr in array2:
        if type(arr) == str:
            check = True
    
        else:
            check = False
            break
    print(check)    
    return check

comp(aaa, bbb)


#Code wars-4
#Complicated. Need to find out
def generateParenthesis(n,  Open,  close,  s, ans):

    if(Open == n and close == n):
        ans.append(s)
        return
 
    if(Open < n):
        generateParenthesis(n, Open+1, close, s+"{", ans)
 
  
    if(close < Open):
        generateParenthesis(n, Open, close + 1, s+"}", ans)
 
 
n = 9

ans = []

generateParenthesis(n, 0, 0, "", ans)
 
for s in ans:
    print(s)


